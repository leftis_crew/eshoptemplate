# E-shop Template
A Shopping basic template built with Angular 4 and Firebase.

## Features

- Only Admin can perform Create/Edit/Delete operations & upload photos of items.
- Normal User can buy items & view their own orders.
- Admin can access all orders
- Login through Facebook & Gmail
- Cart System
- Realtime updates (through Firebase Realtime Database).

## Tech Stuff Used

- Angular 4 *(Frontend)*
- Bootstrap
- Firebase Realtime Database *(Backend)*
- Firebase authentication

## TO DO 
- Implement SQL Database
- Backend Functionality (Replace Firebase)
- Fix Home Page

> To use your own Firebase Database change the api key from [`firebase-key`](./src/private/firebase-key.ts)

